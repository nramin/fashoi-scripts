from urllib.error import HTTPError
from bs4 import BeautifulSoup
import bottlenose
import csv
import sys
import time
from datetime import date
import random
from pymongo import MongoClient
import re

AWS_ACCESS_KEY_ID = "AKIAJTVGMDLN72IZROWQ"
AWS_SECRET_ACCESS_KEY = "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"
AWS_ASSOCIATE_TAG = "shop0f46-20"
KEYWORDS_FILE = "../keywords/keywords.csv"
MONGO_HOST = "localhost"
MONGO_PORT = 27017

def error_handler(err):
    ex = err['exception']
    if isinstance(ex, HTTPError) and ex.code == 503:
        time.sleep(random.expovariate(0.1))
        return True

amazon = bottlenose.Amazon(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_ASSOCIATE_TAG, 
            ErrorHandler=error_handler, Parser=lambda text: BeautifulSoup(text, 'xml'))
mongo_client = MongoClient(MONGO_HOST, MONGO_PORT)
db = mongo_client.fashoi
collection = db.items

def get_image_sets(image_sets_data):
    image_sets = []
    for variant_image in image_sets_data:
        image_set_urls = {}
        small_image = variant_image.find("SmallImage")
        if small_image is not None:
            url = small_image.find("URL")
            if url is not None:
                image_set_urls["smallUrl"] = get_string(url)
        medium_image = variant_image.find("MediumImage")
        if medium_image is not None:
            url = medium_image.find("URL")
            if url is not None:
                image_set_urls["mediumUrl"] = get_string(url)
        large_image = variant_image.find("LargeImage")
        if large_image is not None:
            url = large_image.find("URL")
            if url is not None:
                image_set_urls["largeUrl"] = get_string(url)
        image_sets.append(image_set_urls)
    return image_sets

def get_price(item):
    lowest_new_price = None
    fallback_price = None

    if item.find("LowestNewPrice") is not None:
        lowest_new_price = item.find("LowestNewPrice").find("FormattedPrice")
        if len(lowest_new_price.string.split("$")) == 2:
            return lowest_new_price.string.split("$")[1]
    if item.find("ItemAttributes") is not None:
        if item.find("ItemAttributes").find("FormattedPrice") is not None:
            fallback_price = item.find("ItemAttributes").find("FormattedPrice")
            if len(fallback_price.string.split("$")) == 2:
                return fallback_price.string.split("$")[1]

    return None

def get_string(element):
    if element is not None:
        return element.string
    else:
        return None

def update_unpopular_item(keyword_csv_row):
    keyword = keyword_csv_row[0]
    category = keyword_csv_row[1]
    pages_to_search = keyword_csv_row[2]
    print("Updating unpopular items for " + keyword)
    unpopular_items_by_keyword = collection.find({"keyword": keyword})
    for item in unpopular_items_by_keyword:
        process_item_update(item)

def process_item_update(item):
    asin = item["asin"]

    current_date = date.isoformat(date.today())
    if collection.find({"asin": asin, "prices.date": current_date}).count() == 0:
        print(asin + " is unpopular (outdated price).")
        item_lookup = amazon.ItemLookup(ItemId=asin, ResponseGroup="Images, ItemAttributes, Offers")

        data = {
            "featuredPhoto": {
                "largeUrl": item_lookup.find("LargeImage").find("URL").string,
                "mediumUrl": item_lookup.find("MediumImage").find("URL").string
            },
            "imageSets": get_image_sets(item_lookup.findAll("ImageSet", {"Category":"variant"}))
        }
        collection.update_one({"asin": asin}, {"$set": data})

        item_price = get_price(item_lookup)
        updated_price = {
            "date": current_date,
            "price": item_price
        }
        collection.update_one({"asin": asin}, {"$push": {"prices": updated_price}})
        collection.update_one({"asin": asin}, {"$set": {"currentPrice": item_price}})

        if item_price is not None:
            print(item_price)

def main():
    with open(KEYWORDS_FILE, 'rt', encoding="UTF8") as f:
        reader = csv.reader(f)
        try:
            for row in reader:
                update_unpopular_item(row)
        except csv.Error as e:
            sys.exit('file %s, line %d: %s' % (KEYWORDS_FILE, reader.line_num, e))

if __name__ == "__main__":
    main()