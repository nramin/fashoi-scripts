from urllib.error import HTTPError
from bs4 import BeautifulSoup
import bottlenose
import csv
import sys
import time
from datetime import date
import random
from pymongo import MongoClient
import re

AWS_ACCESS_KEY_ID = "AKIAJTVGMDLN72IZROWQ"
AWS_SECRET_ACCESS_KEY = "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"
AWS_ASSOCIATE_TAG = "shop0f46-20"
KEYWORDS_FILE = "../keywords/keywords.csv"
MONGO_HOST = "localhost"
MONGO_PORT = 27017

def error_handler(err):
    ex = err['exception']
    if isinstance(ex, HTTPError) and ex.code == 503:
        time.sleep(random.expovariate(0.1))
        return True

amazon = bottlenose.Amazon(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_ASSOCIATE_TAG, 
            ErrorHandler=error_handler, Parser=lambda text: BeautifulSoup(text, 'xml'))
mongo_client = MongoClient(MONGO_HOST, MONGO_PORT)
db = mongo_client.fashoi
collection = db.items

def get_feature_descriptions(features):
    feature_description = ""
    for feature in features:
        if feature_description is "":
            feature_description += feature.string
        else:
            feature_description += "\n" + feature.string
    return feature_description
    
def is_item_data_valid(data):
    if data.find("LargeImage") is None:
        return False
    elif data.find("ItemAttributes").find("FormattedPrice") is None:
        return False
    elif data.find("ItemAttributes").find("Binding") is None:
        return False
    else:
        return True

def update_existing_item_fields(existing_item, item):
    asin = item.find("ASIN").string

    # Updating price if item already exists
    if existing_item.get('prices') is not None:
        current_date = date.isoformat(date.today())
        if collection.find({"asin": asin, "prices.date": current_date}).count() == 0:
            price = get_price(item)
            updated_price = {
                "date": current_date,
                "price": price
            }
            collection.update_one({"asin": asin}, {"$push": {"prices": updated_price }})
            collection.update_one({"asin": asin}, {"$set": {"currentPrice": price}})

    # Updating main image urls if item already exists
    updated_featured_image = {
        "largeUrl": item.find("LargeImage").find("URL").string,
        "mediumUrl": item.find("MediumImage").find("URL").string
    }
    collection.update_one({"asin": asin}, {"$set": {"featuredPhoto": updated_featured_image}})

    # Updating image set urls if item already exists
    image_sets = {
        "imageSets": get_image_sets(item.findAll("ImageSet", {"Category":"variant"}))
    }
    collection.update_one({"asin": asin}, {"$set": image_sets})

def get_price(item):
    lowest_new_price = None
    fallback_price = None

    if item.find("LowestNewPrice") is not None:
        lowest_new_price = item.find("LowestNewPrice").find("FormattedPrice")
        if len(lowest_new_price.string.split("$")) == 2:
            return lowest_new_price.string.split("$")[1]

    if item.find("ItemAttributes") is not None:
        if item.find("ItemAttributes").find("FormattedPrice") is not None:
            fallback_price = item.find("ItemAttributes").find("FormattedPrice")
            if len(fallback_price.string.split("$")) == 2:
                return fallback_price.string.split("$")[1]

    return None

def get_image_sets(image_sets_data):
    image_sets = []
    for variant_image in image_sets_data:
        image_set_urls = {}
        small_image = variant_image.find("SmallImage")
        if small_image is not None:
            url = small_image.find("URL")
            if url is not None:
                image_set_urls["smallUrl"] = get_string(url)
        medium_image = variant_image.find("MediumImage")
        if medium_image is not None:
            url = medium_image.find("URL")
            if url is not None:
                image_set_urls["mediumUrl"] = get_string(url)
        large_image = variant_image.find("LargeImage")
        if large_image is not None:
            url = large_image.find("URL")
            if url is not None:
                image_set_urls["largeUrl"] = get_string(url)
        image_sets.append(image_set_urls)
    return image_sets

def get_string(element):
    if element is not None:
        return element.string
    else:
        return None

def lookup_keyword(keyword_csv_row):
    keyword = keyword_csv_row[0]
    category = keyword_csv_row[1]
    pages_to_search = keyword_csv_row[2]
    sort_type = keyword_csv_row[3]
    print(keyword_csv_row)
    for page_number in range(1, int(pages_to_search) + 1):
        results = amazon.ItemSearch(ResponseGroup="Images,ItemAttributes,ItemIds,Offers", 
                    SearchIndex=category, Keywords=keyword, ItemPage=page_number)
        for item in results.find_all('Item'):
            process_keyword_item(keyword, item)

def process_keyword_item(keyword,item):
    print(item.find("ASIN").string)
    if is_item_data_valid(item):
        existing_item = collection.find_one({"asin": item.find("ASIN").string})
        if existing_item is not None:
            # If item does exist, update its info
            print("We found a duplicate of " + item.find("ASIN").string)
            update_existing_item_fields(existing_item, item)
        else:
            # If item is new, add a new document for it
            manufacturer = "Not Available" if item.find("ItemAttributes").find("Manufacturer") \
                            is None else item.find("ItemAttributes").find("Manufacturer").string
            title = item.find("ItemAttributes").find("Title").string
            data = {
                "titleId": re.sub('[^0-9a-zA-Z]+', '-', title.lower()),
                "keyword": keyword,
                "asin": item.find("ASIN").string,
                "binding": get_string(item.find("ItemAttributes").find("Binding")),
                "brand": get_string(item.find("ItemAttributes").find("Brand")),
                "department": get_string(item.find("ItemAttributes").find("Department")),
                "title": title,
                "manufacturer": manufacturer,
                "featureDescriptions": get_feature_descriptions(item.find("ItemAttributes").find_all("Feature")),
                "featuredPhoto": {
                    "largeUrl": item.find("LargeImage").find("URL").string,
                    "mediumUrl": item.find("MediumImage").find("URL").string
                },
                "imageSets": get_image_sets(item.findAll("ImageSet", {"Category":"variant"})),
                "currentPrice": get_price(item),
                "prices": [
                    {
                        "date": date.isoformat(date.today()),
                        "price": get_price(item)
                    }
                ]
            }
            collection.insert_one(data)

def main():
    with open(KEYWORDS_FILE, 'rt', encoding="UTF8") as f:
        reader = csv.reader(f)
        try:
            for row in reader:
                lookup_keyword(row)
        except csv.Error as e:
            sys.exit('file %s, line %d: %s' % (KEYWORDS_FILE, reader.line_num, e))

if __name__ == "__main__":
    main()