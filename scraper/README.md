- pip install lxml
- pip install git+git://github.com/lionheart/bottlenose
- pip install bs4
- pip install pymongo

OR

- python3 -m pip install lxml
- python3 -m pip install git+git://github.com/lionheart/bottlenose
- python3 -m pip install bs4
- python3 -m pip install pymongo